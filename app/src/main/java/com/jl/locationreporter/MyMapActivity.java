package com.jl.locationreporter;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jl.locationreporter.widget.AlertDialogFragment;
import com.jl.locationreporter.widget.ButteryProgressBar;

public class MyMapActivity extends Activity implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        AlertDialogFragment.AlertDialogInterface,
        GoogleMap.OnMyLocationButtonClickListener,
        LocationListener {

    Button sendButton;
    EditText numberField;
    ButteryProgressBar progressBar;

    GoogleMap mMap;
    LocationClient mLocationClient;
    BroadcastReceiver smsReceiver;
    Location lastKnownLocation;
    boolean isRequestingLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        setUpMapIfNeeded();

        progressBar = (ButteryProgressBar) findViewById(R.id.progress);
        numberField = (EditText) findViewById(R.id.numberField);
        sendButton = (Button) findViewById(R.id.sendButton);

        mLocationClient = new LocationClient(this, this, this);

        //if (!Device.isGpsEnabled(this))
        //    showGpsDialog();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mLocationClient.connect();
    }

    @Override
    protected void onStop() {
        mLocationClient.removeLocationUpdates(this);
        mLocationClient.disconnect();
        if (smsReceiver != null)
            unregisterReceiver(smsReceiver);
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {
        /**
         lastKnownLocation = mLocationClient.getLastLocation();
         if (lastKnownLocation == null)// TODO: handle no last known location
         return;
         createMarker(lastKnownLocation);
         */

        requestLocation();
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        hideProgress();
        mMap.clear();
        mLocationClient.removeLocationUpdates(this);
        isRequestingLocation = false;
        lastKnownLocation = location;
        createMarker(location);
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            MapFragment frag = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            mMap = frag.getMap();

            // Check if we were successful in obtaining the map.
            if (mMap != null)
                setUpMap();
            // The Map is verified. It is now safe to manipulate the map.
        }
    }

    private void setUpMap() {
        centerMap(new LatLng(14.6093682, 120.996481), 10, false);
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
    }

    private void requestLocation() {

        if (isRequestingLocation)
            return;

        isRequestingLocation = true;
        LocationRequest mRequest = LocationRequest.create();
        mRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mRequest.setInterval(5);

        mLocationClient.requestLocationUpdates(mRequest, this);
        // TODO stop on pause, continue on resume.. use shared pref to know if location is requested
    }

    private void createMarker(Location loc) {
        LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());
        centerMap(latLng, 12, true);

        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                .title(latLng.latitude + "," + latLng.longitude));
        marker.showInfoWindow();
    }

    private void centerMap(LatLng latLng, int zoom, boolean animate) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
        if (animate)
            mMap.animateCamera(cameraUpdate);
        else
            mMap.moveCamera(cameraUpdate);
    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
        sendButton.setEnabled(true);
    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        sendButton.setEnabled(false);
    }

    private void showGpsDialog() {
        DialogFragment frag = AlertDialogFragment.newInstance(R.string.gps_dialog_message, R.string.gps_dialog_positive, R.string.gps_dialog_negative);
        frag.show(getFragmentManager(), "");
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void sendSms(View view) {

        if (!isValidNumber()) {
            showMessage(getString(R.string.error_number));
            return;
        }

        hideKeyboard(view);
        showSmsDialog();

    }

    private void send(String num, String body) {
        showProgress();

        String SENT = "SMS_SENT";
        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
        smsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                hideProgress();
                showMessage("location sent");
            }
        };
        registerReceiver(smsReceiver, new IntentFilter(SENT));
        SmsManager manager = SmsManager.getDefault();
        manager.sendTextMessage(num, null, body, sentPI, null);
    }

    private boolean isValidNumber() {
        String numString = numberField.getText().toString();
        return !(numString == null || numString.isEmpty() || numString.length() < 11);

    }

    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void showSmsDialog() {
        DialogFragment frag = AlertDialogFragment.newInstance(R.string.dialog_send_body, R.string.send, R.string.cancel);
        frag.show(getFragmentManager(), "sms");
    }

    @Override
    public void onPositive() {
        String number = numberField.getText().toString();
        String body = lastKnownLocation.getLatitude() + "," + lastKnownLocation.getLongitude();
        send(number, body);
        numberField.setText("");
        showMessage("sending...");
    }

    @Override
    public void onNegative() {
        showMessage("cancelled");
    }

    @Override
    public boolean onMyLocationButtonClick() {
        showProgress();
        requestLocation();
        return true;
    }
}
