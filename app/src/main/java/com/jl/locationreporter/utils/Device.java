package com.jl.locationreporter.utils;

import android.content.Context;
import android.location.LocationManager;

public class Device {

    public static boolean isGpsEnabled(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
}
