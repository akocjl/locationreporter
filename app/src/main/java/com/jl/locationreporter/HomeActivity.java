package com.jl.locationreporter;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.jl.locationreporter.utils.Device;
import com.jl.locationreporter.widget.AlertDialogFragment;


public class HomeActivity extends Activity implements AlertDialogFragment.AlertDialogInterface{

    private static final int REQUEST_GPS = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_GPS) {
            if (Device.isGpsEnabled(this))
                goToMap();
            else
                showMessage(getString(R.string.gps_still_off));
        }
    }

    public void locateMyPosition(View view) {
        if (Device.isGpsEnabled(this)) {
            goToMap();
        } else {
            showGpsDialog();
        }
    }

    private void showGpsDialog() {
        DialogFragment frag = AlertDialogFragment.newInstance(R.string.gps_dialog_message, R.string.gps_dialog_positive, R.string.gps_dialog_negative);
        frag.show(getFragmentManager(), "");
    }

    @Override
    public void onPositive() {
        goToSettings();
    }

    @Override
    public void onNegative() {

    }

    private void showMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void goToSettings(){
        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(callGPSSettingIntent, REQUEST_GPS);
    }

    private void goToMap() {
        Intent intent = new Intent(this, MyMapActivity.class);
        startActivity(intent);
    }
}
